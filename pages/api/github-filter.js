const fetch = require('cross-fetch');

const githubFilter = async (req, res) => {
  const {
    query: {startingDate, language = 'all', perPage = 10},
  } = req;

  try {
    const githubRes = await fetch(
      `https://api.github.com/search/repositories?q=language:${language}+created:>${startingDate}&sort=stars&order=desc&per_page=${perPage}`
    );

    const data = await githubRes.json();
    const parsedData = {};

    const parsedItems = data.items.map(
      ({
        html_url: htmlUrl,
        name,
        stargazers_count: stargazersCount,
        description,
        id,
      }) => ({
        htmlUrl,
        name,
        stargazersCount,
        description,
        id,
      })
    );

    parsedData.totalCount = data.total_count;
    parsedData.items = parsedItems;

    res.status(200).json(parsedData);
  } catch (error) {
    throw new Error(error);
  }
};

export default githubFilter;
