import {useState, useEffect} from 'react';
import Layout from '../components/Layout/Layout';
import List from '../components/List/List';

const favorites = () => {
  const [favorites, setFavorites] = useState({
    data: {items: []},
    status: 'loading',
  });

  useEffect(() => {
    if (typeof window !== 'undefined' && localStorage.getItem('favorites')) {
      const savedFavorites = JSON.parse(localStorage.getItem('favorites'));
      setFavorites({
        data: {items: savedFavorites},
        status: 'success',
      });
    } else {
      setFavorites({
        data: {items: []},
        status: 'success',
      });
    }
  }, []);

  return (
    <Layout>
      <h1>My Favorites</h1>
      <List data={favorites.data} status={favorites.status} />
    </Layout>
  );
};

export default favorites;
