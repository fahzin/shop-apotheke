import React from 'react';
import App from 'next/app';
import 'sanitize.css';
import {ThemeProvider} from 'styled-components';
import {theme} from '../assets/styles/theme';

class ShopApotheke extends App {
  componentDidMount() {
    if (!document.documentElement.classList.contains('wf-active')) {
      import('webfontloader').then(WebFont =>
        WebFont.load({
          google: {
            families: ['Hind:400,500,600'],
          },
        })
      );
    }
  }

  render() {
    const {Component, pageProps} = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    );
  }
}

export default ShopApotheke;
