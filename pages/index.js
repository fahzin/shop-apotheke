import {useCallback, useState, useEffect} from 'react';
import Layout from '../components/Layout/Layout';
import useAsync from '../shared/useAsync';
import List from '../components/List/List';
import {fetchRepos} from '../shared/fetchRepos';
import Filters from '../components/Filters/Filters';

const Index = () => {
  const [filters, setFilters] = useState({
    language: 'all',
    perPage: 10,
  });
  const callback = useCallback(() => fetchRepos(filters), [filters]);
  const {data, status} = useAsync(callback);

  const filterData = ({target: {value, name}}) => {
    setFilters(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  useEffect(() => {
    if (typeof window !== 'undefined' && !localStorage.getItem('favorites')) {
      localStorage.setItem('favorites', '[]');
    }
  }, []);

  return (
    <Layout>
      <h1>Most popular repositories of the last 7 days...</h1>
      <Filters filterData={filterData} />
      <List data={data} status={status} />
    </Layout>
  );
};

export default Index;
