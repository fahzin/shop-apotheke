# Shop Apotheke Front-End test (and a little backend)

A ReactJS Application which the user can see the most popular Rpositiories of GitHub of last 7 days.

## Install & Usage

1. Install project dependencies, run the command below:

```bash
yarn
```

2. start the development server:

```bash
yarn dev
```

## Build for production

1. generate a production build:

```bash
yarn build
```

2. start a production server:

```bash
yarn start
```

# Folder Structure

```
/ assets
  / styles
    - theme.js
/ components
  / ComponentName
    - ComponentName.js
    - componentName.styled.js
/ data
  - languages.js
/ pages
  /api
    - github-filter.js
  - _app.js
  - _document.js
  - index.js
  - pageName.js
/ shared
  - fetchRepos.js
  - useAsync.js
  - utils.js
- nexjs.config.js
```
