import axios from 'axios';
import {getDateInThePast} from './utils';

export const fetchRepos = async ({language, perPage}) => {
  // we substract 8 days because github API will see
  // repositories there are 'bigger' than this date which means the date we pass won't count
  const startingDate = getDateInThePast(8);

  const params = {
    startingDate,
    language,
    perPage,
  };

  const {data} = await axios.get('http://localhost:3000/api/github-filter', {
    params,
  });

  return data;
};
