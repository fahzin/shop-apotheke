export const getDateInThePast = daysToRemove => {
  // Get current date
  const date = new Date();
  // Substract days from current date
  date.setDate(date.getDate() - daysToRemove);

  // Get the date formated to use in the
  const dateFormated = date.toISOString().slice(0, 10);

  return dateFormated;
};
