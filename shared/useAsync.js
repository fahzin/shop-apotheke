/* eslint-disable no-shadow */
import {useReducer, useEffect} from 'react';

const reducer = (state, action) => {
  switch (action.type) {
    case 'start': {
      return {
        data: null,
        error: null,
        status: 'loading',
      };
    }
    case 'resolved': {
      return {
        data: action.data,
        error: null,
        status: 'resolved',
      };
    }
    case 'failed': {
      return {
        data: null,
        status: 'failed',
        error: action.error,
      };
    }
    default:
      throw new Error(`Action ${action.type} is unhandled.`);
  }
};

const useAsync = fn => {
  const [{data, status, error}, dispatch] = useReducer(reducer, {
    data: null,
    error: null,
    status: 'loading',
  });

  useEffect(() => {
    let cancelled = false;
    dispatch({type: 'start'});

    fn().then(
      data => {
        if (cancelled) return;
        dispatch({type: 'resolved', data});
      },
      err => {
        if (cancelled) return;
        dispatch({type: 'failed', error: err});
      }
    );

    return () => {
      cancelled = true;
    };
  }, [fn]);

  return {data, error, status};
};

export default useAsync;
