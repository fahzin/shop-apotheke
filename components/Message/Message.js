import * as S from './message.styled';

const Message = ({children}) => <S.Message>{children}</S.Message>;

export default Message;
