import styled from 'styled-components';

export const Message = styled.div`
  width: 100%;
  float: left;
  overflow: hidden;
  padding: 10px;
  font-family: ${({theme}) => theme.fonts.primary};
  position: relative;
`;
