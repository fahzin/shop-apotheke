import styled from 'styled-components';

export const ItemWrapper = styled.div`
  width: 100%;
  float: left;
  border-bottom: 1px solid #e3e3e3;
  overflow: hidden;
  padding: 10px 10px 10px 40px;
  font-family: ${({theme}) => theme.fonts.primary};
  position: relative;

  &:last-child {
    border: 0;
  }

  h1 {
    font-size: ${({theme}) => theme.px2rem(18)};
    margin: 0;

    a {
      color: ${({theme}) => theme.colors.primary};
      transition: color 0.3s;

      &:hover {
        color: ${({theme}) => theme.colors.secondary};
      }
    }
  }

  p {
    font-size: ${({theme}) => theme.px2rem(16)};
    color: ${({theme}) => theme.colors.text};
    margin-bottom: 0;
  }
`;

export const Favorite = styled.div`
  font-size: ${({theme}) => theme.px2rem(20)};
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  left: 10px;
  cursor: pointer;
  color: ${({theme, active}) =>
    active ? theme.colors.active : theme.colors.inactive};
  transition: color 0.3s;

  &:hover {
    color: ${({theme}) => theme.colors.active};
  }
`;
