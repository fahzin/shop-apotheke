import {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHeart} from '@fortawesome/free-solid-svg-icons';
import * as S from './listItem.styled';

const ListItem = ({
  data: {id, htmlUrl, name, stargazersCount, description},
}) => {
  const [favorite, setFavorite] = useState(false);

  const addFavorite = () => {
    const currentItem = {htmlUrl, name, stargazersCount, description, id};
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const cureentItemIndex = favorites.findIndex(item => item.id === id);
    let newFavorites = [...favorites, currentItem];

    if (cureentItemIndex !== -1) {
      favorites.splice(cureentItemIndex, 1);
      newFavorites = favorites;
    }

    localStorage.setItem('favorites', JSON.stringify(newFavorites));
    setFavorite(!favorite);
  };

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const cureentItemIndex = favorites.findIndex(item => item.id === id);

    if (cureentItemIndex !== -1) {
      setFavorite(true);
    }
  }, []);

  return (
    <S.ItemWrapper>
      <S.Favorite active={favorite} onClick={addFavorite}>
        <FontAwesomeIcon icon={faHeart} />
      </S.Favorite>
      <h1>
        <a href={htmlUrl} target={'_blank'} rel='noopener noreferrer'>
          {name}
        </a>{' '}
        (⭐ {stargazersCount})
      </h1>
      <p>{description}</p>
    </S.ItemWrapper>
  );
};

ListItem.propTypes = {
  data: PropTypes.exact({
    name: PropTypes.string.isRequired,
    htmlUrl: PropTypes.string.isRequired,
    stargazersCount: PropTypes.number.isRequired,
    description: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
};

export default ListItem;
