import PropTypes from 'prop-types';
import Navigation from '../Navigation/Navigation';
import * as S from './layout.styled';

const Layout = ({children}) => (
  <>
    <Navigation />
    <S.Wrapper>{children}</S.Wrapper>
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
