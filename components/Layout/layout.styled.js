import styled from 'styled-components';

export const Wrapper = styled.main`
  width: 100%;
  max-width: 1200px;
  margin: auto;
  padding: 0 20px;

  h1 {
    font-family: ${({theme}) => theme.fonts.primary};
    font-size: ${({theme}) => theme.px2rem(22)};
    width: 100%;
    float: left;
    margin: 20px 0;
  }
`;
