import PropTypes from 'prop-types';
import {languages} from '../../data/languages';
import * as S from './filters.styled';

const Filters = ({filterData}) => (
  <S.FiltersWrapper>
    Filters:{' '}
    <S.Select name={'language'} onChange={evt => filterData(evt)}>
      <option value='all'>All languages</option>
      {languages.map(language => (
        <option key={language.value} value={language.value}>
          {language.text}
        </option>
      ))}
    </S.Select>
    <S.Select name={'perPage'} onChange={evt => filterData(evt)}>
      <option value='10'>10 items per page</option>
      <option value='50'>50 items per page</option>
      <option value='100'>100 items per page</option>
    </S.Select>
  </S.FiltersWrapper>
);

Filters.propTypes = {
  filterData: PropTypes.func.isRequired,
};

export default Filters;
