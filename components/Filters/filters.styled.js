import styled from 'styled-components';

export const FiltersWrapper = styled.div`
  width: 100%;
  float: left;
  border-radius: 5px;
  overflow: hidden;
  font-family: ${({theme}) => theme.fonts.primary};
`;

export const Select = styled.select`
  width: auto;
  border-radius: 5px;
  margin: 0 10px;
  font-family: ${({theme}) => theme.fonts.primary};
  font-size: ${({theme}) => theme.px2rem(14)};
`;
