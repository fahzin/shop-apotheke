import Link from 'next/link';
import {withRouter} from 'next/router';
import PropTypes from 'prop-types';
import * as S from './navigation.styled';

const Navigation = ({router: {pathname}}) => (
  <S.Navigation>
    <S.NavigationWrapper>
      <Link href='/'>
        <a className={pathname === '/' ? 'active' : ''}>Home</a>
      </Link>
      <Link href='/favorites'>
        <a className={pathname === '/favorites' ? 'active' : ''}>
          All Favorites
        </a>
      </Link>
    </S.NavigationWrapper>
  </S.Navigation>
);

Navigation.propTypes = {
  router: PropTypes.object.isRequired,
};

export default withRouter(Navigation);
