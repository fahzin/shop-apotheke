import styled from 'styled-components';

export const Navigation = styled.nav`
  width: 100%;
  float: left;
  background-color: ${({theme}) => theme.colors.primary};
`;

export const NavigationWrapper = styled.nav`
  width: 100%;
  max-width: 1200px;
  margin: 20px auto;
  font-family: ${({theme}) => theme.fonts.primary};
  padding: 0 20px;

  a {
    color: ${({theme}) => theme.colors.link};
    transition: color 0.3s;
    text-transform: uppercase;
    text-decoration: none;
    margin-right: 20px;
    position: relative;
    transition: color 0.3s;

    &:after {
      position: absolute;
      content: '';
      height: 1px;
      bottom: 0;
      left: 50%;
      width: 0;
      background-color: white;
      transition: width 0.3s, left 0.3s;
    }

    &:hover,
    &.active {
      color: ${({theme}) => theme.colors.LinkActive};

      &:after {
        width: 100%;
        left: 0;
      }
    }
  }
`;
