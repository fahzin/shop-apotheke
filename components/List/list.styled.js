import styled from 'styled-components';

export const ListWrapper = styled.div`
  width: 100%;
  float: left;
  border: 1px solid #e3e3e3;
  border-radius: 5px;
  overflow: hidden;
  margin: 20px 0;
`;
