import PropTypes from 'prop-types';
import ListItem from '../ListItem/ListItem';
import Message from '../Message/Message';
import * as S from './list.styled';

const List = ({data, status}) => (
  <S.ListWrapper>
    {status === 'loading' ? (
      <Message>Loading repositiories...</Message>
    ) : (
      <>
        {status === 'failed' ? (
          <Message>
            Something went wrong, please try again later or contact us on
            contact@support.de
          </Message>
        ) : (
          <>
            {data.items.length === 0 ? (
              <Message>No repositories found :(</Message>
            ) : (
              data.items.map(item => <ListItem key={item.id} data={item} />)
            )}
          </>
        )}
      </>
    )}
  </S.ListWrapper>
);

List.defaultProps = {
  data: null,
};

List.propTypes = {
  data: PropTypes.exact({
    items: PropTypes.array,
    totalCount: PropTypes.number,
  }),
  status: PropTypes.string.isRequired,
};

export default List;
