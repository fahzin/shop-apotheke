/* eslint-disable id-length */
import {css} from 'styled-components';

export const sizes = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

export const colors = {
  primary: '#000000',
  text: '#757575',
  secondary: '#005091',
  inactive: '#e3e3e3',
  active: '#ea0100',
  link: '#6b6b6b',
  LinkActive: '#ffffff',
};

export const fonts = {
  primary: "'Hind', sans-serif",
};

export const hexToRgb = hex => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/iu.exec(hex);
  return result
    ? `${parseInt(result[1], 16)}, ${parseInt(result[2], 16)}, ${parseInt(
        result[3],
        16
      )}`
    : '';
};

export const px2rem = pxl => `${pxl / 16}rem`;

export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label] / 16}em) {
      ${css(...args)};
    }
  `;
  return acc;
}, {});

export const theme = {
  name: 'shop-apotheke',
  sizes,
  colors,
  media,
  fonts,
  px2rem,
  hexToRgb,
};
